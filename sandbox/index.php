<?php

// HACK(ssandriesser): DIY file server
if (isset($_GET['file'])) {
    echo file_get_contents(urldecode($_GET['file']));
    exit;
}

$postAction = isset($_POST['action']) ? $_POST['action'] : null;

// Upload action
if ($postAction === 'upload') {

    $uploadDir = __DIR__ . '/upload';
    $filename = basename($_FILES['img']['name']);
    $uploadfile = $uploadDir.'/'.$filename;


    $coordinates = isset($_POST['coordinates']) ? json_decode($_POST['coordinates']) : null;


    var_dump(array(

        '$coordinates' => $coordinates,

        'upload_dir' => $uploadDir,
        'upload_filename' => $filename,
        'size' => $_FILES['img']['size'],
        'files' => $_FILES,
    ));

    if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {
        echo "File is valid, and was successfully uploaded.\n";
    } else {
        echo "Possible file upload attack!\n";
    }

    exit;
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Title</title>
    <style>
        * {
            box-sizing: border-box;
        }
        .canvas {
            border: 1px solid rgba(0,0,0,0.12);
            display: none;
        }
        .text-right {
            text-align: right;
        }
        .w-50 {
            width: 50%;
        }
        .w-40 {
            width: 40%;
        }
        .w-60 {
            width: 60%;
        }
        .flex {
            display: flex;
        }
        .text-danger {
            color: red;
        }
    </style>
</head>
<body class="flex">
    <div class="w-40">
        <input type="file" id="img-file" onchange="setBackgroundImg()">
        <pre><code id="output"></code></pre>
    </div>
    <div class="w-60">
        <div>
            <button onclick="canvas.add(createDefaultRect(), true)">rect</button>
            <button onclick="canvas.add(createDefaultCensor(), true)">censor</button>
            <button id="delete" onclick="canvas.removeActive()" disabled>delete</button>
            |
            <button onclick="canvas.rotateBgLeft()">left</button>
            <button onclick="canvas.rotateBgRight()">right</button>
            |
            <button onclick="canvas.toggleLayer('shapes')">toggle shapes</button>
            <button onclick="canvas.toggleLayer('censor')">toggle censor</button>
            |
            <button onclick="upload()">upload</button>
        </div>
        <canvas id="canvas" class="canvas" style="box-sizing: border-box;"></canvas>
    </div>

<script src="/index.php?file=<?= urlencode('../node_modules/axios/dist/axios.min.js') ?>"></script>
<script src="/index.php?file=<?= urlencode('../dist/image-editor.js') ?>"></script>
<script>


    function upload() {
        let data = new FormData();
        let output = document.getElementById('output');

        const dataURL = canvas.toDataURL();

        canvas.createFile(dataURL).then(function(file) {
            data.append('action', 'upload');
            data.append('img', file);
            data.append('coordinates', JSON.stringify(canvas.exportCoordinates()));

            console.log(file);

            axios.post('/index.php', data)
                .then(function (res) {
                    output.className = '';
                    output.innerHTML = res.data;
                })
                .catch(function (err) {
                    output.className = 'text-danger';
                    output.innerHTML = err.message;
                });
        });
    }


    function createDefaultRect() {
        return new ImageEditor.Rect({
            top: 20,
            left: 20,
            width: 80,
            height: 40
        });
    }

    function createDefaultCensor() {
        return new ImageEditor.CensorRect({
            top: 20,
            left: 20,
            width: 80,
            height: 40
        });
    }

    function setBackgroundImg() {
        let files = document.getElementById('img-file');

        function readFiles(file) {
            if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {
                let reader = new FileReader();

                reader.addEventListener("load", function () {
                    console.log(file);
                    let image = new Image();
                    image.title = file.name;
                    image.file = file;
                    image.src = this.result;
                    canvas.setBackgroundImg(image);
                }, false);
                reader.readAsDataURL(file);
            }

        }

        if (files.files.length) {
            [].forEach.call(files.files, readFiles.bind(this));
        }
    }

    let canvas = new ImageEditor.Canvas('canvas', {
        // minWidth: 300,
        // maxWidth: 500,
        // width: 400,
        // responsive: false
    });

    canvas.on('setactive', function () {
        document.getElementById('delete').disabled = false;
    });

    canvas.on('unsetactive', function () {
        document.getElementById('delete').disabled = true;
    });

    // let rect = new ImageEditor.CensorRect({
    //     top: 20,
    //     left: 40,
    //     width: 40,
    //     height: 60
    // });
    //
    // let rect2 = new ImageEditor.Rect({
    //     top: 50,
    //     left: 70,
    //     width: 40,
    //     height: 20,
    //     borderColor: 'green'
    // });

</script>
</body>
</html>