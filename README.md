# Image Editor

The ImageEditor is in an early stage of development. So, don't use it in a production system. The API will may be changed in release version.

> Contributors are welcome.

## Installation

Install with [yarn](https://yarnpkg.com)

```bash
yarn add https://gitlab.com/JohnPirate/image-editor#master
```

## Usage

```javascript 
const canvas = new ImageEditor.Canvas('canvas', {
    width: 400,
    height: 250,
});

let rect = new ImageEditor.Rect({
    top: 20,
    left: 40,
    width: 40,
    height: 60
});

canvas.add(rect);

canvas.renderAll();
```

## Contributing

Any type of feedback, pull request or issue is welcome.

### Tests

**Unit tests**
```bash
node_modules/.bin/mocha-webpack --webpack-config webpack.tests.config.js --require tests/setup.js tests
```

**Code lint**
```bash
node_modules/.bin/eslint src --fix
```

## License

The ImageEditor is licensed under the MIT license.