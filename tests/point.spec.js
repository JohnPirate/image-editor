import Point from '../src/Point';

describe ('Point', () => {

    it('tests setter of it selves', () => {

        let p1 = new Point();
        expect(p1.x).toBe(0);
        expect(p1.y).toBe(0);

        let p2 = new Point(1,1);
        expect(p2.x).toBe(1);
        expect(p2.y).toBe(1);

        p1.setFromPoint(p2);
        expect(p1.x).toBe(p2.x);
        expect(p1.y).toBe(p2.y);
        expect(p1).not.toBe(p2);

        p1.setX(2);
        expect(p1.x).toBe(2);
        expect(p1.y).toBe(1);

        p1.setY(3);
        expect(p1.x).toBe(2);
        expect(p1.y).toBe(3);

        p1.setXY(4,5);
        expect(p1.x).toBe(4);
        expect(p1.y).toBe(5);
    });

    it('tests its comparison operators', () => {

        let p1 = new Point();
        let p2 = new Point(1,1);
        let p3 = new Point(-1,-1);

        expect(p1.eq(p1.clone())).toBe(true);
        expect(p1.eq(p2)).toBe(false);

        expect(p1.lt(p1.clone())).toBe(false);
        expect(p1.lt(p3)).toBe(false);
        expect(p1.lt(p2)).toBe(true);

        expect(p1.lte(p1.clone())).toBe(true);
        expect(p1.lte(p3)).toBe(false);
        expect(p1.lte(p2)).toBe(true);

        expect(p1.gt(p1.clone())).toBe(false);
        expect(p1.gt(p3)).toBe(true);
        expect(p1.gt(p2)).toBe(false);

        expect(p1.gte(p1.clone())).toBe(true);
        expect(p1.gte(p3)).toBe(true);
        expect(p1.gte(p2)).toBe(false);
    });

    it('tests add a Point', () => {
        let p1 = new Point(1,2);
        let p2 = new Point(3,4);

        let p3 = p1.add(p2);

        expect(p1.x).toBe(1);
        expect(p1.y).toBe(2);
        expect(p3.x).toBe(4);
        expect(p3.y).toBe(6);
        expect(p1).not.toBe(p3);
    });

    it('tests addEquals a Point', () => {
        let p1 = new Point(1,2);
        let p2 = new Point(3,4);

        let p3 = p1.addEquals(p2);

        expect(p1.x).toBe(4);
        expect(p1.y).toBe(6);
        expect(p3.x).toBe(4);
        expect(p3.y).toBe(6);
        expect(p1).toBe(p3);
    });

    it('tests subtract a Point', () => {
        let p1 = new Point(1,2);
        let p2 = new Point(3,4);

        let p3 = p1.subtract(p2);

        expect(p1.x).toBe(1);
        expect(p1.y).toBe(2);
        expect(p3.x).toBe(-2);
        expect(p3.y).toBe(-2);
        expect(p1).not.toBe(p3);
    });

    it('tests subtractEquals a Point', () => {
        let p1 = new Point(1,2);
        let p2 = new Point(3,4);

        let p3 = p1.subtractEquals(p2);

        expect(p1.x).toBe(-2);
        expect(p1.y).toBe(-2);
        expect(p3.x).toBe(-2);
        expect(p3.y).toBe(-2);
        expect(p1).toBe(p3);
    });

    it('tests multiplyScalar ', () => {
        let p1 = new Point(1,2);
        let s = 3;

        let p2 = p1.multiplyScalar(s);

        expect(p1.x).toBe(1);
        expect(p1.y).toBe(2);
        expect(p2.x).toBe(3);
        expect(p2.y).toBe(6);
        expect(p1).not.toBe(p2);
    });

    it('tests multiplyScalarEquals', () => {
        let p1 = new Point(1,2);
        let s = 3;

        let p2 = p1.multiplyScalarEquals(s);

        expect(p1.x).toBe(3);
        expect(p1.y).toBe(6);
        expect(p2.x).toBe(3);
        expect(p2.y).toBe(6);
        expect(p1).toBe(p2);
    });

    it('tests divideScalar ', () => {
        let p1 = new Point(3,6);
        let s = 3;

        let p2 = p1.divideScalar(s);

        expect(p1.x).toBe(3);
        expect(p1.y).toBe(6);
        expect(p2.x).toBe(1);
        expect(p2.y).toBe(2);
        expect(p1).not.toBe(p2);
    });

    it('tests divideScalarEquals', () => {
        let p1 = new Point(3,6);
        let s = 3;

        let p2 = p1.divideScalarEquals(s);

        expect(p1.x).toBe(1);
        expect(p1.y).toBe(2);
        expect(p2.x).toBe(1);
        expect(p2.y).toBe(2);
        expect(p1).toBe(p2);
    });

    it('tests distanceFrom a Point', () => {
        let p1 = new Point(4,1);
        let p2 = new Point(4,-1);

        let distance = p1.distanceFrom(p2);

        expect(distance).toBe(2);
    });

    it('tests midPointFrom a Point', () => {
        let p1 = new Point(2,2);
        let p2 = new Point(6,4);

        let p3 = p1.midPointFrom(p2);

        expect(p3.x).toBe(4);
        expect(p3.y).toBe(3);
    });

    it('tests clone it selves', () => {
        let p1 = new Point(2,2);
        let p2 = p1.clone();

        expect(p2.x).toBe(p1.x);
        expect(p2.y).toBe(p1.y);
        expect(p2).not.toBe(p1);
    });

    // it('', () => {});
});