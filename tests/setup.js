require('jsdom-global')();
global.expect = require('expect');

document.body.innerHTML = `<div id="not-a-canvas-element"></div>`;

// HACK(ssandriesser): overwrite getElementById function to use the canvas context in unit tests
const { createCanvas } = require('canvas');
const d = document;
const f = document.getElementById;
document.getElementById = param => param.toString() === 'canvas'
    ? (() => {
        let canvas = createCanvas(400, 200);
        let canvasElement = d.createElement('canvas');
        canvasElement.id = 'canvas';
        let parent = document.createElement('div');
        parent.offsetWidth = 700;
        parent.appendChild(canvasElement);
        canvasElement.parentElement = parent;
        canvasElement.getContext = () => { return canvas.getContext('2d'); };
        return canvasElement;
      })()
    : f.call(d, param);

