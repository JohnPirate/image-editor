import '../src/ImageEditor';
import Canvas from '../src/Canvas';
import Rect from '../src/Rect';
import CensorRect from '../src/CensorRect';

describe ('Canvas', () => {

    const globalCanvas = new Canvas('canvas');

    it('throws an error if no id is given when create new Canvas object', () => {
        expect(() => {new Canvas()}).toThrow(/No id given/);
    });

    it('throws an error if no element is found in DOM for given id', () => {
        expect(() => {
            new Canvas('no-dom-element');
        }).toThrow(/No element found in DOM for id no-dom-element/);
    });

    it('throws an error if DOM element is not a canvas element', () => {
        expect(() => {
            new Canvas('not-a-canvas-element');
        }).toThrow(/Element is not from type 'canvas'/);
    });

    it('throws an error if you add an object that is not from type CanvasObject', () => {
        expect(() => {
            globalCanvas.add({});
        }).toThrow(/Object is not from type CanvasObject/);
    });

    it('throws an error if you remove an object that is not from type CanvasObject', () => {
        expect(() => {
            globalCanvas.remove({});
        }).toThrow(/Object is not from type CanvasObject/);
    });

    it('throws an error if you set an active element that is not from type CanvasObject', () => {
        expect(() => {
            globalCanvas.setActiveElement({});
        }).toThrow(/Object is not from type CanvasObject/);
    });

    it('throws an error if you try to hide an unknown layer', () => {
        expect(() => {
            globalCanvas.hideLayer('foo');
        }).toThrow(/Layer foo not found./);
    });

    it('throws an error if you try to show an unknown layer', () => {
        expect(() => {
            globalCanvas.showLayer('foo');
        }).toThrow(/Layer foo not found./);
    });

    it('add\'s a CanvasObject that is not from type CensorRect to the shapes layer', () => {

        const canvas = new Canvas('canvas');

        expect(canvas._layers.shapes.data.length).toBe(0);

        canvas.add(new Rect());

        expect(canvas._layers.shapes.data.length).toBe(1);
    });

    it('add\'s a CanvasObject that is from type CensorRect to the censor layer', () => {

        const canvas = new Canvas('canvas');

        expect(canvas._layers.censor.data.length).toBe(0);

        canvas.add(new CensorRect());

        expect(canvas._layers.censor.data.length).toBe(1);
    });

    it('remove\'s a CanvasObject that is not from type CensorRect from the shapes layer', () => {

        const canvas = new Canvas('canvas');
        const canvasObject = new Rect();

        canvas.add(canvasObject);

        expect(canvas._layers.shapes.data.length).toBe(1);

        canvas.remove(canvasObject);

        expect(canvas._layers.shapes.data.length).toBe(0);
    });

    it('remove\'s a CanvasObject that is from type CensorRect from the censor layer', () => {

        const canvas = new Canvas('canvas');
        const canvasObject = new CensorRect();

        canvas.add(canvasObject);

        expect(canvas._layers.censor.data.length).toBe(1);

        canvas.remove(canvasObject);

        expect(canvas._layers.censor.data.length).toBe(0);
    });

    it('clear\'s the shapes layer', () => {

        const canvas = new Canvas('canvas');
        const canvasObject = new Rect();

        canvas.add(canvasObject);
        canvas.add(canvasObject);

        expect(canvas._layers.shapes.data.length).toBe(2);

        canvas.clearShapes();

        expect(canvas._layers.shapes.data.length).toBe(0);
    });

    it('clear\'s the censor layer', () => {

        const canvas = new Canvas('canvas');
        const canvasObject = new CensorRect();

        canvas.add(canvasObject);
        canvas.add(canvasObject);

        expect(canvas._layers.censor.data.length).toBe(2);

        canvas.clearCensor();

        expect(canvas._layers.censor.data.length).toBe(0);
    });

    // it('', () => {});
});