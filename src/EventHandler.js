/**
 * @class EventHandler
 */
export default class EventHandler {
    /**
     * Constructor of EventHandler
     */
    constructor() {
        this.events = {};
    }

    /**
     * @param {String}   name
     * @param {Function} callback
     */
    listen(name, callback) {
        if (!Object.prototype.hasOwnProperty.call(this.events, name)) {
            this.events[name] = [];
        }
        this.events[name].push(callback);
    }

    /**
     * @param {String}                     name
     * @param {String|Object|Number|Array} data
     */
    emit(name, data) {
        if (!Object.prototype.hasOwnProperty.call(this.events, name)) {
            return;
        }
        this.events[name].forEach((callback) => {
            callback(data);
        });
    }
}
