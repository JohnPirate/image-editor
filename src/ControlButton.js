/**
 * @class ControlButton
 */
export default class ControlButton {
    /**
     * Constructor of ControlButton
     *
     * @param {Point}  point
     * @param {String} name
     * @param {Object} options
     */
    constructor(point, name, options = {}) {
        this.point = point;
        this.name = name;
        this.radius = options.radius || 5;
        this.color = options.color || 'blue';
    }

    /**
     * @param {CanvasRenderingContext2D} ctx
     */
    render(ctx) {
        ctx.fillStyle = this.color;
        ctx.beginPath();
        ctx.arc(this.point.x, this.point.y, this.radius, 0, Math.PI * 2, false);
        ctx.closePath();
        ctx.fill();
    }

    /**
     * @param {Point} point
     *
     * @return {Boolean}
     */
    isMouseOver(point) {
        const distance = this.point.distanceFrom(point);
        return distance <= this.radius;
    }
}
