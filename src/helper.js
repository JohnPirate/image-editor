/* eslint no-bitwise: ["error", { "allow": ["|", "&", "^", ">>"] }] */
/* eslint no-mixed-operators: ["error", {
    "groups": [["&&", "||"]],
    "allowSamePrecedence": true
}] */
/* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["object"] }] */
/**
 * Merge two objects
 *
 * @param {Object} obj
 * @param {Object} src
 *
 * @return {Object}
 */
export function objectMerge(obj, src) {
    const mergedObject = obj;
    Object.keys(src).forEach((key) => { mergedObject[key] = src[key]; });
    return mergedObject;
}

/**
 * Wraps an HTMLElement over an other HTMLElement
 *
 * @param {HTMLElement} el
 * @param {HTMLElement} wrapper
 */
export function wrap(el, wrapper) {
    el.parentNode.insertBefore(wrapper, el);
    wrapper.appendChild(el);
}

/**
 * @param {String} dataURL
 * @param {String} name
 * @param {String} mime
 *
 * @return {Promise}
 */
export function dataUrlToFile(dataURL, name, mime) {
    return new Promise((resolve) => {
        const blobBin = atob(dataURL.split(',')[1]);
        const array = [];
        for (let i = 0; i < blobBin.length; i += 1) {
            array.push(blobBin.charCodeAt(i));
        }
        resolve(new File([new Uint8Array(array)], name, { type: mime }));
    });
}

/**
 * Uuid v4 implementation
 *
 * @return {String}
 */
export function uuidv4() {
    if (typeof crypto !== 'undefined' && typeof Uint8Array !== 'undefined') {
        return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(
            /[018]/g,
            c => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16),
        );
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        const r = Math.random() * 16 | 0;
        const v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

/**
 * Bind an event to an Object
 *
 * @param {Object}   object
 * @param {String}   type
 * @param {Function} callback
 *
 * @return {void}
 */
export function addEvent(object, type, callback) {
    if (object === null || typeof object === 'undefined') {
        return;
    }
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent(`on${type}`, callback);
    } else {
        object[`on${type}`] = callback;
    }
}
