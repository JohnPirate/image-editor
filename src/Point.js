/**
 * @class Point
 */
export default class Point {
    /**
     * Constructor of Point
     *
     * @param {Number} x
     * @param {Number} y
     */
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    /**
     * @param {Point} point
     *
     * @return {Number}
     */
    distanceFrom(point) {
        const dx = point.x - this.x;
        const dy = point.y - this.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    /**
     * Linear interpolation between this point and another point
     *
     * @param {Point}  point
     * @param {Number} t from 0 to 1
     *
     * @return {Point}
     */
    lerp(point, t = 0.5) {
        const rt = Math.max(Math.min(1, t), 0);
        return new Point(
            this.x + (point.x - this.x) * rt,
            this.y + (point.y - this.y) * rt,
        );
    }

    /**
     * @param {Point} point
     *
     * @return {Point}
     */
    midPointFrom(point) {
        return this.lerp(point);
    }

    /**
     * @param {Point} point
     *
     * @return {Point}
     */
    add(point) {
        return new Point(this.x + point.x, this.y + point.y);
    }

    /**
     * @param {Point} point
     *
     * @return {Point}
     */
    addEquals(point) {
        this.x += point.x;
        this.y += point.y;
        return this;
    }

    /**
     * @param {Point} point
     *
     * @return {Point}
     */
    subtract(point) {
        return new Point(this.x - point.x, this.y - point.y);
    }

    /**
     * @param {Point} point
     *
     * @return {Point}
     */
    subtractEquals(point) {
        this.x -= point.x;
        this.y -= point.y;
        return this;
    }

    /**
     * @param {Number} scalar
     *
     * @return {Point}
     */
    multiplyScalar(scalar) {
        return new Point(this.x * scalar, this.y * scalar);
    }

    /**
     * @param {Number} scalar
     *
     * @return {Point}
     */
    multiplyScalarEquals(scalar) {
        this.x *= scalar;
        this.y *= scalar;
        return this;
    }

    /**
     * @param {Number} scalar
     *
     * @return {Point}
     */
    divideScalar(scalar) {
        return new Point(this.x / scalar, this.y / scalar);
    }

    /**
     * @param {Number} scalar
     *
     * @return {Point}
     */
    divideScalarEquals(scalar) {
        this.x /= scalar;
        this.y /= scalar;
        return this;
    }

    /**
     * @param {Point} point
     *
     * @return {boolean}
     */
    eq(point) {
        return (this.x === point.x && this.y === point.y);
    }

    /**
     * @param {Point} point
     *
     * @return {boolean}
     */
    lt(point) {
        return (this.x < point.x && this.y < point.y);
    }

    /**
     * @param {Point} point
     *
     * @return {boolean}
     */
    lte(point) {
        return (this.x <= point.x && this.y <= point.y);
    }

    /**
     * @param {Point} point
     *
     * @return {boolean}
     */
    gt(point) {
        return (this.x > point.x && this.y > point.y);
    }

    /**
     * @param {Point} point
     *
     * @return {boolean}
     */
    gte(point) {
        return (this.x >= point.x && this.y >= point.y);
    }

    /**
     * @return {String}
     */
    toString() {
        return `${this.x},${this.y}`;
    }

    /**
     * @param {Point} point
     *
     * * @return {Point}
     */
    setFromPoint(point) {
        this.x = point.x;
        this.y = point.y;
        return this;
    }

    /**
     * @param {Number} x
     * @param {Number} y
     *
     * @return {Point}
     */
    setXY(x, y) {
        this.x = x;
        this.y = y;
        return this;
    }

    /**
     * @param {Number} x
     *
     * @return {Point}
     */
    setX(x) {
        this.x = x;
        return this;
    }

    /**
     * @param {Number} y
     *
     * @return {Point}
     */
    setY(y) {
        this.y = y;
        return this;
    }

    /**
     * @return {Point}
     */
    clone() {
        return new Point(this.x, this.y);
    }
}
