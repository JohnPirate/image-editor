/* eslint no-underscore-dangle: ["error", { "allowAfterSuper": true ,"allowAfterThis": true}] */

import CanvasObject from './CanvasObject';
import Point from './Point';

/**
 * @class Rect
 */
export default class Rect extends CanvasObject {
    /**
     * Constructor of Rect
     *
     * @param {object} options
     */
    constructor(options = {}) {
        super(options);
        this.width = options.width || 30;
        this.height = options.height || 30;
        this.border = options.border || 1;
        this.borderColor = options.borderColor || 'red';
        this._initEvents();
    }

    /**
     * @protected
     */
    _initEvents() {
        super._initEvents();

        /**
         * @param {CanvasEvent} e
         */
        this.on('mouse:move', (e) => {
            const mo = this._isMouseOver(e);
            if (mo && !this.mouseOver) {
                this.mouseOver = true;
                this.fire('mouseover', e);
            } else if (!mo && this.mouseOver) {
                this.mouseOver = false;
                this.fire('mouseout', e);
            }

            if (this.isActive && this.mouseDownCtrl) {
                const p = new Point(e.canvas.x, e.canvas.y);
                this.changeSize(p, this.activeCtrlBtName);
                this.fire('forcerender');
            } else if (this.isActive && this.mouseDown) {
                const p = new Point(e.canvas.x, e.canvas.y);
                p.subtractEquals(this.mouseDownPointRelative);
                this.moveTo(p);
                this.fire('forcerender');
            }
        });

        /**
         * @param {CanvasEvent} e
         */
        this.on('mouse:down', (e) => {
            if (this.mouseOver) {
                this.mouseDown = true;
                this.mouseDownPoint = new Point(e.canvas.x, e.canvas.y);
                this.mouseDownPointRelative = new Point(
                    e.canvas.x - this.left,
                    e.canvas.y - this.top,
                );
                this.fire('mousedown', e);
            }
        });

        /**
         * @param {CanvasEvent} e
         */
        this.on('mouse:down:ctrl', (e) => {
            this.mouseDownPoint = new Point(e.canvas.x, e.canvas.y);
            this.mouseDownPointRelative = new Point(
                e.canvas.x - this.left,
                e.canvas.y - this.top,
            );
            this.mouseDownCtrl = true;
            this.activeCtrlBtName = e.ctrlBtnName;
            this.fire('mousedown:ctrl', e);
        });

        /**
         * @param {CanvasEvent} e
         */
        this.on('mouse:up', (e) => {
            this.mouseDown = false;
            this.mouseDownCtrl = false;
            this.mouseDownPoint = null;
            this.mouseDownPointRelative = null;
            this.activeCtrlBtName = null;

            this._normalizeValues();

            if (this.mouseOver) {
                this.fire('mouseup', e);
            }
        });
    }

    _normalizeValues() {
        if (this.height < 0) {
            this.height = Math.round(Math.abs(this.height));
            this.top = Math.round(Math.abs(this.top) - this.height);
        }
        if (this.width < 0) {
            this.width = Math.round(Math.abs(this.width));
            this.left = Math.round(Math.abs(this.left) - this.width);
        }
    }

    /**
     * @param {CanvasEvent} e
     * @return {boolean}
     * @protected
     */
    _isMouseOver(e) {
        return e.canvas.y >= this.top && e.canvas.y <= (this.top + this.height)
            && e.canvas.x >= this.left && e.canvas.x <= (this.left + this.width);
    }

    /**
     * @param {CanvasEvent} e
     * @return {boolean}
     */
    isClicked(e) {
        return this._isMouseOver(e);
    }

    /**
     * @param {CanvasRenderingContext2D} ctx
     * @private
     */
    _render(ctx) {
        super._render(ctx);
        ctx.strokeStyle = this.borderColor;
        ctx.lineWidth = this.border;
        ctx.strokeRect(
            this.left,
            this.top,
            this.width,
            this.height,
        );
    }
}
