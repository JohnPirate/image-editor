/* eslint no-underscore-dangle: ["error", { "allowAfterSuper": true ,"allowAfterThis": true}] */

import Rect from './Rect';

/**
 * @class CensorRect
 */
export default class CensorRect extends Rect {
    /**
     * Constructor of CensorRect
     *
     * @param {object} options
     */
    constructor(options = {}) {
        super(options);
        this.backgroundColor = 'black';
        this.borderColor = 'black';
    }

    /**
     * @param {CanvasRenderingContext2D} ctx
     * @private
     */
    _render(ctx) {
        ctx.fillStyle = this.backgroundColor;
        ctx.fillRect(
            this.left,
            this.top,
            this.width,
            this.height,
        );
    }
}
