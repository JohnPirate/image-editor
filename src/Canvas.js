/* eslint no-underscore-dangle: ["error", {
     "allowAfterSuper": true,
     "allowAfterThis": true,
     "allow": [
        "_createCanvasEvent",
        "_layers",
     ]
 }]
*/

import CanvasEvent from './events/CanvasEvent';
import { addEvent, dataUrlToFile, objectMerge } from './helper';
import CensorRect from './CensorRect';
import CanvasObject from './CanvasObject';

/**
 * @class Canvas
 */
export default class Canvas {
    /**
     * Constructor of Canvas
     *
     * @param {String} id
     * @param {Object} options
     */
    constructor(id, options = {}) {
        if (!id) {
            throw new Error('No id given');
        }

        /**
         * {String}
         */
        this.id = id;

        /**
         * @type {HTMLCanvasElement}
         */
        this.canvasElement = document.getElementById(this.id);
        if (!this.canvasElement) {
            throw new Error(`No element found in DOM for id ${this.id}`);
        }
        if (this.canvasElement.nodeName.toLowerCase() !== 'canvas') {
            throw new Error('Element is not from type \'canvas\'');
        }

        /**
         * {Object}
         */
        this.options = {
            width: 400,
            height: 250,
            maxWidth: null,
            minWidth: null,
            responsive: true,
        };
        this.options = objectMerge(this.options, options);

        /**
         * @type {CanvasRenderingContext2D}
         */
        this.ctx = this.canvasElement.getContext('2d');

        /**
         * @deprecated
         * @type {Array}
         * @protected
         */
        this._objects = [];

        /**
         * @protected
         *
         * @type {Object}
         */
        this._layers = {
            backgroundImage: {
                visible: true,
                data: null,
            },
            censor: {
                visible: true,
                data: [],
            },
            shapes: {
                visible: true,
                data: [],
            },
            selection: {
                visible: true,
                data: null,
            },
        };

        /**
         * @deprecated
         *
         * @type {null|HTMLImageElement}
         */
        this.backgroundImage = null;

        /**
         * Angel of the background image
         *
         * @type {Number}
         */
        this.angelBackgroundImage = 0;

        /**
         * @type {null|CanvasObject}
         */
        this.activeObject = null;

        /**
         * @type {number}
         */
        this.devicePixelRatio = window.devicePixelRatio || 1;

        /**
         * The scale between the original imported image sizes and the rendered image sizes
         *
         * @type {number}
         */
        this.scale = 1;

        /**
         * @type {EventHandler}
         */
        this.ceh = window.ImageEditor.EventHandler;

        this._initEvents();

        this._initCanvas();
    }

    /**
     * Return all shapes
     *
     * @return {Array}
     */
    get objects() {
        return this._layers.shapes.data;
    }

    /**
     * Return the current width of the canvas
     *
     * @return {Number}
     */
    get width() {
        return this.canvasElement.width / this.devicePixelRatio;
    }

    /**
     * Set the with of the canvas
     *
     * @param {Number} value
     */
    set width(value) {
        this.canvasElement.width = value * this.devicePixelRatio;
        this.canvasElement.style.width = `${value}px`;
    }

    /**
     * Get the current height of the canvas
     *
     * @return {Number}
     */
    get height() {
        return this.canvasElement.height / this.devicePixelRatio;
    }

    /**
     * Set the height of the canvas
     *
     * @param {Number} value
     */
    set height(value) {
        this.canvasElement.height = value * this.devicePixelRatio;
        this.canvasElement.style.height = `${value}px`;
    }

    /**
     * @protected
     */
    _initCanvas() {
        this.ctx.scale(this.devicePixelRatio, this.devicePixelRatio);

        const vm = this;
        new Promise((resolve) => {
            resolve(vm.resizeCanvas());
        }).then(() => {
            vm.canvasElement.style.display = 'block';
        });
    }

    /**
     * Get the visibility of an specific layer
     *
     * @param {String} layer
     *
     * @return {Boolean}
     * @protected
     */
    _getLayerVisibility(layer) {
        if (!Object.prototype.hasOwnProperty.call(this._layers, layer)) {
            throw new Error(`Layer ${layer} not found.`);
        }
        return this._layers[layer].visible;
    }

    /**
     * Set the visibility of an specific layer
     *
     * @param {String} layer
     * @param {Boolean} visible
     * @protected
     */
    _setLayerVisibility(layer, visible) {
        if (!Object.prototype.hasOwnProperty.call(this._layers, layer)) {
            throw new Error(`Layer ${layer} not found.`);
        }
        this._layers[layer].visible = visible;
        this.unsetActiveElement();
        this.fire('forcerender');
    }

    /**
     * Check if an specific layer is visible
     *
     * @param {String} layer
     *
     * @return {Boolean}
     */
    isLayerVisible(layer) {
        return this._getLayerVisibility(layer);
    }

    /**
     * Hide a specific layer
     *
     * @param {String} layer
     */
    hideLayer(layer) {
        this._setLayerVisibility(layer, false);
    }

    /**
     * Show a specific layer
     *
     * @param {String} layer
     */
    showLayer(layer) {
        this._setLayerVisibility(layer, true);
    }

    /**
     * Toggle the visibility of an specific layer
     *
     * @param {String} layer
     */
    toggleLayer(layer) {
        if (this.isLayerVisible(layer)) {
            this.hideLayer(layer);
        } else {
            this.showLayer(layer);
        }
    }

    /**
     * Set the canvas options
     *
     * @param {Object} options
     */
    setOptions(options) {
        this._initCanvas(options);
    }

    /**
     * @param {MouseEvent} e
     *
     * @return {CanvasEvent}
     * @protected
     */
    _createCanvasEvent(e) {
        const canvasRect = this.canvasElement.getBoundingClientRect();
        return new CanvasEvent(e, canvasRect.top, canvasRect.left);
    }

    /**
     * Initialize events
     * @protected
     */
    _initEvents() {
        const vm = this;

        /**
         * @param {MouseEvent} e
         */
        function canvasOnMouseDown(e) {
            const ce = vm._createCanvasEvent(e);
            vm.fire('mouse:down', ce);
            let canvasObject;
            if (vm.hasActiveElement()) {
                canvasObject = vm.getActiveElement();
                const ctrlBtn = canvasObject.getHoveredCtrlBtn(ce);
                if (ctrlBtn) {
                    // FIXME(ssandriesser): take an CtrlBtnEvent instead
                    ce.ctrlBtnName = ctrlBtn.name;
                    vm.fire('mouse:down:ctrl', ce);
                    return;
                }

                if (canvasObject.isClicked(ce)) {
                    return;
                }
            }

            canvasObject = null;

            if (vm._layers.censor.visible) {
                canvasObject = vm._layers.censor.data.find(object => object.isClicked(ce));
            }

            if (vm._layers.shapes.visible) {
                canvasObject = vm._layers.shapes.data.find(object => object.isClicked(ce))
                             || canvasObject;
            }

            if (canvasObject) {
                vm.setActiveElement(canvasObject);
            } else {
                vm.unsetActiveElement();
            }
        }
        addEvent(this.canvasElement, 'mousedown', canvasOnMouseDown);

        /**
         * @param {MouseEvent} e
         */
        function canvasOnMouseUp(e) {
            const ce = vm._createCanvasEvent(e);
            vm.fire('mouse:up', ce);
        }
        addEvent(this.canvasElement, 'mouseup', canvasOnMouseUp);

        /**
         * @param {MouseEvent} e
         */
        function canvasOnMouseMove(e) {
            const ce = vm._createCanvasEvent(e);
            vm.fire('mouse:move', ce);
        }
        addEvent(this.canvasElement, 'mousemove', canvasOnMouseMove);

        /**
         * @param {MouseEvent} e
         */
        function canvasOnMouseOver(e) {
            const ce = vm._createCanvasEvent(e);
            vm.fire('mouse:over', ce);
        }
        addEvent(this.canvasElement, 'mouseover', canvasOnMouseOver);

        /**
         * @param {MouseEvent} e
         */
        function canvasOnMouseOut(e) {
            const ce = vm._createCanvasEvent(e);
            vm.fire('mouse:out', ce);
        }
        addEvent(this.canvasElement, 'mouseout', canvasOnMouseOut);

        /**
         * @var {Number} resizeDelayTimer
         */
        let resizeDelayTimer;

        function canvasOnResize() {
            if (vm.options.responsive) {
                window.clearTimeout(resizeDelayTimer);
                resizeDelayTimer = window.setTimeout(() => {
                    vm.resizeCanvas();
                }, 230);
            }
        }
        addEvent(window, 'resize', canvasOnResize);

        this.on('forcerender', () => {
            vm.renderAll();
        });
    }

    /**
     * Render the canvas
     */
    renderAll() {
        const vm = this;

        this.clear();

        if (this._layers.backgroundImage.visible && this._layers.backgroundImage.data) {
            Canvas.renderImg({
                ctx: this.ctx,
                img: this._layers.backgroundImage.data,
                width: this.width,
                height: this.height,
                angel: this.angelBackgroundImage,
            });
        }

        // Render censor shapes
        if (this._layers.censor.visible) {
            this._layers.censor.data.forEach((object) => {
                if (vm.getActiveElement() !== object) {
                    object.render(vm.ctx);
                }
            });
        }

        // Render Shapes
        if (this._layers.shapes.visible) {
            this._layers.shapes.data.forEach((object) => {
                if (vm.getActiveElement() !== object) {
                    object.render(vm.ctx);
                }
            });
        }


        // Render Active element
        if (this.hasActiveElement()) {
            this.getActiveElement().render(this.ctx);
        }

        // Render Selection
        // TODO(ssandriesser): add render logic for selection feature
    }

    /**
     * @param {Object} parameters
     */
    static renderImg(parameters) {
        const {
            ctx, img, width, height, angel,
        } = parameters;
        ctx.save();
        ctx.translate(width / 2, height / 2);
        ctx.rotate(angel * (Math.PI / 180));
        if ((angel % 180) === 0) {
            ctx.drawImage(
                img,
                -width / 2,
                -height / 2,
                width,
                height,
            );
        } else {
            ctx.drawImage(
                img,
                -height / 2,
                -width / 2,
                height,
                width,
            );
        }
        ctx.restore();
    }

    /**
     * Clear the canvas
     */
    clear() {
        this.ctx.clearRect(
            0,
            0,
            this.width,
            this.height,
        );
    }

    /**
     * Remove all elements from a layer
     *
     * @param {String} layer
     * @protected
     */
    _clearLayer(layer) {
        if (!Object.prototype.hasOwnProperty.call(this._layers, layer)) {
            throw new Error(`Layer ${layer} not found.`);
        }
        this._layers[layer].data = [];
        this.unsetActiveElement();
        this.fire('forcerender');
    }

    /**
     * Remove all elements from shapes layer
     */
    clearShapes() {
        this._clearLayer('shapes');
    }

    /**
     * Remove all elements from censor layer
     */
    clearCensor() {
        this._clearLayer('censor');
    }

    /**
     * Listen on a CanvasEvent
     *
     * @param {String} name
     *
     * @param {Function} callback
     */
    on(name, callback) {
        this.ceh.listen(name, callback);
    }

    /**
     * Fire a CanvasEvent
     *
     * @param {String} name
     *
     * @param {Object|Array|String|Number|null} data
     */
    fire(name, data = null) {
        this.ceh.emit(name, data);
    }

    /**
     * Set an active element
     *
     * @param {CanvasObject} cObj
     *
     * @throws {Error}
     */
    setActiveElement(cObj) {
        Canvas.throwIfNoCanvasObjectIsGiven(cObj);

        if (this.activeObject) {
            this.activeObject.unsetActive();
        }

        cObj.setActive();
        this.activeObject = cObj;
        this.fire('setactive');
        this.fire('forcerender');
    }

    /**
     * @return {null|CanvasObject}
     */
    getActiveElement() {
        return this.activeObject;
    }

    /**
     * Check if canvas has an active object
     *
     * @return {boolean}
     */
    hasActiveElement() {
        return this.activeObject !== null;
    }

    /**
     * Unset the active element
     */
    unsetActiveElement(remove = false) {
        if (this.activeObject) {
            this.activeObject.unsetActive();
            if (remove) {
                this.remove(this.activeObject);
            }
            this.activeObject = null;
            this.fire('unsetactive');
            this.fire('forcerender');
        }
    }

    /**
     * Check if given object is type from CanvasObject
     *
     * @param {Object} cObj
     *
     * @throws {Error}
     */
    static throwIfNoCanvasObjectIsGiven(cObj) {
        if (!(cObj instanceof CanvasObject)) {
            throw new Error('Object is not from type CanvasObject');
        }
    }

    /**
     * Add a new object to the canvas
     *
     * @param {CanvasObject} cObj
     * @param {boolean}      setActive
     *
     * @throws {Error}
     */
    add(cObj, setActive = false) {
        Canvas.throwIfNoCanvasObjectIsGiven(cObj);

        if (cObj instanceof CensorRect) {
            this._layers.censor.data.push(cObj);
        } else {
            this._layers.shapes.data.push(cObj);
        }

        if (setActive) {
            this.setActiveElement(cObj);
        } else {
            this.fire('forcerender');
        }
    }

    /**
     * Remove an object from canvas
     *
     * @param {CanvasObject} cObj
     *
     * @throws {Error}
     */
    remove(cObj) {
        Canvas.throwIfNoCanvasObjectIsGiven(cObj);

        if (cObj instanceof CensorRect) {
            this._layers.censor.data.splice(this._layers.censor.data.indexOf(cObj), 1);
        } else {
            this._layers.shapes.data.splice(this._layers.shapes.data.indexOf(cObj), 1);
        }
        this.fire('forcerender');
    }

    /**
     * Remove and unset Element
     */
    removeActive() {
        this.unsetActiveElement(true);
    }

    /**
     * Set the background image
     *
     * @param {HTMLImageElement} file
     */
    setBackgroundImg(file) {
        const vm = this;
        new Promise((resolve) => {
            resolve(vm._layers.backgroundImage.data = file);
        }).then(() => {
            vm.angelBackgroundImage = 0;
            vm.resizeCanvas();
        });
    }

    /**
     * Rotate the BackgroundImage clockwise over 90 degrees
     */
    rotateBgLeft() {
        this._rotateBackgroundImg(-90);
    }

    /**
     * Rotate the BackgroundImage counter-clockwise over 90 degrees
     */
    rotateBgRight() {
        this._rotateBackgroundImg(90);
    }

    /**
     * @param {Number} angel
     * @protected
     */
    _rotateBackgroundImg(angel) {
        this.angelBackgroundImage += angel;
        if (this.angelBackgroundImage >= 360) {
            this.angelBackgroundImage = 0;
        }
        this.resizeCanvas();
    }

    /**
     * Resize the Canvas
     */
    resizeCanvas() {
        const vm = this;
        const parent = this.canvasElement.parentElement;
        const oldWidth = Number(this.width);
        const wRef = this.options.responsive ? parent.offsetWidth : this.options.width;
        const wMin = this.options.minWidth !== null ? this.options.minWidth : 0;
        const wMax = this.options.maxWidth !== null ? this.options.maxWidth : wRef + wMin;

        this.width = Math.min(Math.max(wMin, wRef), wMax);

        if (this._layers.backgroundImage.data) {
            if ((this.angelBackgroundImage % 180) === 0) {
                this.scale = this.width / this._layers.backgroundImage.data.naturalWidth;
                this.height = this._layers.backgroundImage.data.naturalHeight * this.scale;
            } else {
                this.scale = this.width / this._layers.backgroundImage.data.naturalHeight;
                this.height = this._layers.backgroundImage.data.naturalWidth * this.scale;
            }
        } else {
            this.height = Math.floor(this.width / 16 * 9);
        }

        /**
         * @param {CanvasObject} obj
         */
        function resizeShapes(obj) {
            obj.scale(oldWidth / vm.width);
        }
        this._layers.shapes.data.forEach(resizeShapes);
        this._layers.censor.data.forEach(resizeShapes);

        this.fire('forcerender');
    }

    /**
     * Create a dataUrl from canvas image
     *
     * @return {string}
     */
    toDataURL() {
        const vm = this;

        /** @var {HTMLCanvasElement} exportCanvas */
        const exportCanvas = document.createElement('canvas');
        exportCanvas.width = this._layers.backgroundImage.data.naturalWidth;
        exportCanvas.height = this._layers.backgroundImage.data.naturalHeight;
        const exportCtx = exportCanvas.getContext('2d');
        Canvas.renderImg({
            ctx: exportCtx,
            img: this._layers.backgroundImage.data,
            width: exportCanvas.width,
            height: exportCanvas.height,
            angel: this.angelBackgroundImage,
        });

        // TODO(ssandriesser): refactor this to render a given list of layers
        // Render censor shapes
        if (this._layers.censor.visible) {
            this._layers.censor.data.forEach((object) => {
                // NOTE(ssandriesser): white is easier to handle than black for the OCR
                exportCtx.fillStyle = 'white';
                exportCtx.fillRect(
                    object.left / vm.scale,
                    object.top / vm.scale,
                    object.width / vm.scale,
                    object.height / vm.scale,
                );
            });
        }
        return exportCanvas.toDataURL();
    }

    /**
     * Creates a File from Canvas. The File contains the Image and Censor information.
     * The exported ImageFile has the original size of imported Image.
     *
     * @param {String} dataUrl
     *
     * @return {Promise}
     */
    createFile(dataUrl = null) {
        let dataUrlIntern = dataUrl;

        // FIXME(ssandriesser): validate dataUrl
        if (!dataUrlIntern) {
            dataUrlIntern = this.toDataURL();
        }

        return dataUrlToFile(
            dataUrlIntern,
            this._layers.backgroundImage.data.file.name,
            this._layers.backgroundImage.data.file.type,
        );
    }

    /**
     * Export the coordinates from the process shapes.
     *
     * @return {Array}
     */
    exportCoordinates() {
        const vm = this;
        const coordinates = [];

        this._layers.shapes.data.forEach((object) => {
            coordinates.push({
                id: object.id,
                x: Math.round(object.top / vm.scale),
                y: Math.round(object.left / vm.scale),
                width: Math.round(object.width / vm.scale),
                height: Math.round(object.height / vm.scale),
            });
        });

        return coordinates;
    }
}
