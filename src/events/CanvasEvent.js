import Point from '../Point';

/**
 * @class CanvasEvent
 */
export default class CanvasEvent {
    /**
     * Constructor of CanvasEvent
     * @param {MouseEvent|WheelEvent} e
     * @param {number} offsetTop
     * @param {number} offsetLeft
     */
    constructor(e, offsetTop, offsetLeft) {
        this.isTrusted = e.isTrusted;

        /**
         * Position in document
         * @type {Point}
         */
        this.page = CanvasEvent.getPositionInPage(e);

        /**
         * Position in DOM
         * @type {Point}
         */
        this.client = new Point(e.clientX, e.clientY);

        /**
         * Position in Canvas
         * @type {Point}
         */
        this.canvas = this.page
            .subtract(new Point(offsetLeft, offsetTop));

        this.altKey = e.altKey;
        this.ctrlKey = e.ctrlKey;
        this.shiftKey = e.shiftKey;
        this.button = e.button;
        this.buttons = e.buttons;
        this.metaKey = e.metaKey;
    }

    /**
     * @param {MouseEvent|WheelEvent} e
     *
     * @return {Point}
     */
    static getPositionInPage(e) {
        if (e.pageX !== undefined && e.pageY !== undefined) {
            return new Point(e.pageX, e.pageY);
        }
        return new Point(
            e.clientX + document.body.scrollLeft
                + document.documentElement.scrollLeft,
            e.clientY + document.body.scrollTop
                + document.documentElement.scrollTop,
        );
    }
}
