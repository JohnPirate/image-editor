import Canvas from './Canvas';
import EventHandler from './EventHandler';
import Rect from './Rect';
import CensorRect from './CensorRect';
import Point from './Point';

window.ImageEditor = {
    Canvas,
    Rect,
    CensorRect,
    Point,

    EventHandler: new EventHandler(),
};
