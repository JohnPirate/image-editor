/* eslint no-underscore-dangle: ["error", { "allowAfterSuper": true ,"allowAfterThis": true}] */
/* eslint class-methods-use-this: ["error", {
    "exceptMethods": ["_initEvents", "_isMouseOver", "_render", "isClicked"] }]
*/
/* eslint no-unused-vars: ["error", { "args": "none" }] */

import EventHandler from './EventHandler';
import Point from './Point';
import ControlButton from './ControlButton';
import { uuidv4 } from './helper';

/**
 * @class CanvasObject
 */
export default class CanvasObject {
    constructor(options = {}) {
        this.id = uuidv4();

        this.top = options.top || 10;
        this.left = options.left || 10;

        /**
         * Width of the element
         * @type {number}
         */
        this.width = 0;

        /**
         * Height of the element
         * @type {number}
         */
        this.height = 0;

        /**
         * Is mouse over flag
         * @type {boolean}
         */
        this.mouseOver = false;

        /**
         * Is mouse down over element flag
         * @type {boolean}
         */
        this.mouseDown = false;

        /**
         * Is mouse down over control button
         * @type {boolean}
         */
        this.mouseDownCtrl = false;

        /**
         * Is element active flag
         * @type {boolean}
         */
        this.isActive = false;

        /**
         * Point where the user pressed down the mouse
         * @type {null|Point}
         */
        this.mouseDownPoint = null;

        /**
         * Relative point btw element point (top|left) and mouse down point
         * @type {null|Point}
         */
        this.mouseDownPointRelative = null;

        /**
         * The active ControlButton
         * @type {null|String}
         */
        this.activeCtrlBtName = null;

        /**
         * Canvas EventHandler
         * @type {EventHandler}
         */
        this.ceh = window.ImageEditor.EventHandler;

        /**
         * Element EventHandler
         * @type {EventHandler}
         */
        this.oeh = new EventHandler();
    }

    get absWidth() {
        return Math.abs(this.width);
    }

    get absHeight() {
        return Math.abs(this.height);
    }

    /**
     * @protected
     */
    _initEvents() {

    }

    /**
     * @param {CanvasEvent} e
     * @protected
     */
    _isMouseOver(e) {
        throw new Error('Method must be implemented.');
    }

    /**
     * @param {CanvasRenderingContext2D} ctx
     * @protected
     */
    _render(ctx) {
    }

    isClicked() {
        throw new Error('Method must be implemented');
    }

    setActive() {
        this.isActive = true;
    }

    unsetActive() {
        this.isActive = false;
    }

    on(name, callback) {
        switch (name) {
        case 'mouseover':
        case 'mouseout':
        case 'mousedown':
        case 'mouseup':
            this.oeh.listen(name, callback.bind(this));
            break;
        default:
            this.ceh.listen(name, callback.bind(this));
        }
    }

    fire(name, data) {
        switch (name) {
        case 'mouseover':
        case 'mouseout':
        case 'mousedown':
        case 'mouseup':
            this.oeh.emit(name, data);
            break;
        default:
            this.ceh.emit(name, data);
        }
    }

    /**
     * @param {CanvasEvent} e
     *
     * @return {ControlButton|null}
     */
    getHoveredCtrlBtn(e) {
        const ctrlBtns = this._getCtrlBtns();

        // NOTE(ssandriesser): return if first found
        return Object.values(ctrlBtns).find(ctrlBtn => ctrlBtn.isMouseOver(e.canvas))
            || null;
    }

    /**
     * @return {Array}
     * @private
     */
    _getCtrlBtns() {
        const right = this.left + this.width;


        const bottom = this.top + this.height;
        const opt = {
            radius: 5,
        };

        const btns = [];

        btns.push(new ControlButton(new Point(this.left, this.top), 'lt', opt));
        btns.push(new ControlButton(new Point(this.left, bottom), 'lb', opt));
        btns.push(new ControlButton(new Point(right, this.top), 'rt', opt));
        btns.push(new ControlButton(new Point(right, bottom), 'rb', opt));

        if (this.absHeight > 5 * opt.radius) {
            const middleY = this.top + (this.height / 2);
            btns.push(new ControlButton(new Point(this.left, middleY), 'lm', opt));
            btns.push(new ControlButton(new Point(right, middleY), 'rm', opt));
        }

        if (this.absWidth > 5 * opt.radius) {
            const middleX = this.left + (this.width / 2);
            btns.push(new ControlButton(new Point(middleX, this.top), 'mt', opt));
            btns.push(new ControlButton(new Point(middleX, bottom), 'mb', opt));
        }

        return btns;
    }

    /**
     * @param {CanvasRenderingContext2D} ctx
     * @private
     */
    _renderActiveCtrl(ctx) {
        if (!this.isActive) {
            return;
        }
        const ctrlBtns = this._getCtrlBtns();
        ctrlBtns.forEach((ctrlBtn) => {
            ctrlBtn.render(ctx);
        });
    }

    /**
     * @param {Point} point
     */
    moveTo(point) {
        this.top = point.y;
        this.left = point.x;
    }

    changeSizeTo(point) {
        this.height += (this.top - point.y);
        this.width += (this.left - point.x);
        this.top = point.y;
        this.left = point.x;
    }

    changeSize(point, ctrlBtnName) {
        switch (ctrlBtnName) {
        case 'lt':
            this.height += (this.top - point.y);
            this.width += (this.left - point.x);
            this.top = point.y;
            this.left = point.x;
            break;

        case 'mt':
            this.height += (this.top - point.y);
            this.top = point.y;
            break;

        case 'rt':
            this.height += (this.top - point.y);
            this.width = point.x - this.left;
            this.top = point.y;
            break;

        case 'lm':
            this.width += (this.left - point.x);
            this.left = point.x;
            break;

        case 'rm':
            this.width = point.x - this.left;
            break;

        case 'lb':
            this.height = point.y - this.top;
            this.width += (this.left - point.x);
            this.left = point.x;
            break;

        case 'mb':
            this.height = point.y - this.top;
            break;

        case 'rb':
            this.height = point.y - this.top;
            this.width = point.x - this.left;
            break;

        default:
            // TODO(ssandriesser): error handling
            break;
        }
    }

    /**
     * @param {Point} point
     */
    moveToX(point) {
        this.top = point.x;
    }

    /**
     * @param {Point} point
     */
    moveToY(point) {
        this.left = point.y;
    }

    /**
     * @param {Number} scale
     */
    scale(scale) {
        this.height /= scale;
        this.width /= scale;
        this.top /= scale;
        this.left /= scale;
    }

    /**
     * @param {CanvasRenderingContext2D} ctx
     */
    render(ctx) {
        this._render(ctx);
        this._renderActiveCtrl(ctx);
    }
}
